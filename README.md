## Gin rest api with mongodb 
### Jin framework + MongoDB + JWT + email sender

```bash
making path for go:
$ export PATH=$PATH:/usr/local/go/bin

Install dependencies: 
$ go get github.com/gin-gonic/gin # the web framework
$ go get github.com/joho/godotenv # environment variables
$ go get gopkg.in/mgo.v2 # mongo driver 
$ go get golang.org/x/crypto/bcrypt # bcrypt
$ go get github.com/dgrijalva/jwt-go # JWT
$ go get github.com/sendgrid/sendgrid-go # Sendgrid.com email sender 
$ go get github.com/PuerkitoBio/goquery 

Run MongoDB docker:

$ sudo docker-compsoe up
$ sudo docker-compsoe down 

To use in MongoDB compass:

`mongodb://root:rootpassword@localhost:27017/test?authSource=admin`

create .env file:
export DBUSER=root
export DBPWD=rootpassword
export DATABASE=localhost
export DATABASE_NAME=test
export SECRET_KEY=secret_key2021
export SENDGRID_API_KEY=here_api_key_value

Launch application:
$ go run app.go 
``` 


```bash
Login existing user 
POST http://localhost:5000/api/v1/login 

{
    "email": "dukenukem@gmail.com", 
    "password": "123123123" 
}

Register new user
POST http://localhost:5000/api/v1/signup 

{
    "name": "Duke Nukem", 
    "email": "dukenukem@mail.com", 
    "password": "123123123" 
}
```